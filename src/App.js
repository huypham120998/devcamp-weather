import logo from './logo.svg';
import './style.css';
import Search from './components/Search';
function App() {
  return (
    <div className='App'>
      <div className='App-header'>
        <Search />
      </div>
    </div>
  );
}

export default App;
