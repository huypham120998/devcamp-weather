import cloudy from "../assets/img/cloudy.svg"
import Rain from "../assets/img/rain.svg"
import Clear from "../assets/img/sky.svg"
import React, { useEffect, useState } from "react";
const Search1 = () => {
  const [style, setStyle] = React.useState(false);
  const [weather, setWeather] = React.useState("");
  const [input, setInput] = React.useState("");
  const [placeholder, setPlaceholder] = useState("Enter City Name");
  const inputChange = (event) => {
    setInput(event.target.value);
}
  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      fetchAPIWeather("https://api.openweathermap.org/data/2.5/forecast?q=" + e.target.value + "&appid=7e101cd8821df641d62df8af6fc333e2");
    }
  }
  const fetchAPIWeather = (url) => {
    fetch(url)
         .then(res => res.json())
         .then(data => {
              console.log(data);
              if (data.cod !== '400' && data.cod !== '404') {
                setWeather(data);
                console.log(weather);
                setStyle(true);
                  //  setPlaceholder("Enter City Name");
              }
              else {
                   setPlaceholder("City was not found, try again");
              }
              setInput("");
         })

}

  const getImageWeather = (weather) => {
    if (weather == "Clouds") {
      return cloudy;
    }
    if (weather == "Rain") {
      return Rain;
    }
    if (weather == "Clear") {
      return Clear;
    }
  }
  var imgWeatherToDay = "";
  var weatherStatusToday = "";
  var weatherTempToday = 0;
  if (weather.list) {
    weatherStatusToday = weather.list[0].weather[0].main;
    weatherTempToday = Math.round(weather.list[0].main.temp - 273.15);
    imgWeatherToDay = getImageWeather(weather.list[0].weather[0].main);
  }
  var dayInWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
  var date = new Date();
  var current_day = date.getDay();
  return (
    <div className="main">
      <div className="inner-main">
        <h1 style={{ display: style ? "none" : "flex" }} className="title">Weather Forecast</h1>
        <img src={cloudy} alt="sun" style={style ? { visibility: "visible", opacity: 1 } : { visibility: "hidden", opacity: 0 }} />
        <div className="today" style={style ? { visibility: "visible", opacity: 1 } : { visibility: "hidden", opacity: 0 }} ><span>Today</span><h1>{weatherStatusToday} </h1><p>Temperature: {weatherTempToday}°C</p><p></p></div>
      </div>
      <input style={{ top: style ? "-300px" : "20px" }} className="city-input" onKeyDown={handleKeyDown}  onChange={inputChange} value={input} type="text" placeholder= {placeholder} />
      <ul className="weather-box-list" style={style ? { display: "flex" } : { display: "none"}} >
        {
          weather.list ?
            weather.list.map((element, index) => {
              if (index > 0 && index < 5) {
                return <div key={index} className="weather-box">
                  <h1>{dayInWeek[current_day + index]}</h1>
                  <img src={getImageWeather(element.weather[0].main)} alt="sun" />
                  <span className="temp">23°C</span>
                </div>
              }
            })
            : ""
        }
      </ul>
    </div>
  )
}
export default Search1;